<!DOCTYPE HTML>

<html lang= "es">
<head>
    <title>Registro de Alumnos</title>
	<meta charset="utf-8">
	<link rel= "stylesheet" type="text/css" href="estilo.css">
</head>


<body id="principal">
<header>
    <div class="banda">
	    <span class="inline">
		    <img src= "https://cdn2.cocinadelirante.com/sites/default/files/styles/gallerie/public/images/2019/08/receta-mazapan-para-pastel.jpg" width="205" height="150">
			<img src= "https://www.flores.ninja/wp-content/uploads/2017/05/Flores.jpg" width="210" height="150">
			<img src= "https://i.pinimg.com/originals/73/73/ca/7373ca91e50abae367ad5d0835777191.jpg" width="205" height="150">
			<img src= "https://66.media.tumblr.com/33bbdf066c07f915615d85e7ff8d72c8/tumblr_mosenvL9TP1snihqzo1_500.jpg" width="205" height="150">
			<img src= "https://www.nicepng.com/png/detail/37-373513_clipart-resolution-15001500-imagenes-de-tumblr-mariposas.png" width="206" height="150">
			<img src= "https://static.vix.com/es/sites/default/files/styles/m/public/l/luna-rosa-llena.jpg" width="206" height="150">
			<img src= "https://previews.123rf.com/images/astronight/astronight1708/astronight170800002/83523617-hermosa-ilustraci%C3%B3n-vectorial-con-fresa-fondo-de-fresa-rosa-patrones-sin-fisuras-con-frutas-de-jard%C3%ADn-fon.jpg" width="200" height="150">
		</span>
		<div class="block">
		<a href="info.php" type="submit" class="button_active" value="Registro">Inicio</a>
		<a href="login.php" type="submit" class="button_active" value= <?php session_destroy();?>>Cerrar Sesión</a>
		</div>
	</div>
</header>

    <div class="container">
	    <div class="container_hijo">
		<h1 id="nombrePag">Formato para el registro</h1>
            <form action="procesar_formulario.php" method="POST">
	        	<div class="block"><input type="number" name="num_cta" placeholder="Número de Cuenta" id="numCuenta"></div>
	        	<div class="block"><input type="text" name="nombre" placeholder="Nombre del Usuario" id="nomUs"></div>
	        	<div class="block"><input type="text" name="primer_apellido" placeholder="Apellido Paterno" id="iapPa"></div>
	        	<div class="block"><input type="text" name="segundo_apellido" placeholder="Apellido Materno" id="iapMa"></div>               
                <div class="block">
		    	    <label for="male">
		    	        <input type="radio" id="hombre" name="genero" value="H">   
		    	        <i class="form-icon"></i> Hombre
				    </label> 
		    	    <label for="female">
		    		    <input type="radio" id="mujer" name="genero" value="M">
		                <i class="form-icon"></i> Mujer
					</label>
		    	    <label for="other">
		    		    <input type="radio" id="otro" name="genero" value="O">
					    <i class="form-icon"></i> Otro
					</label>
		        </div>
	        	<div class="block"><input type="date" name="fec_nac" placeholder="Fecha de Nacimiento"></div>
	        	<div class="block"><input type="password" name="contrasena" placeholder="Contraseña" id="contrasenia"></div>
	        	<div class="block"><input type="submit" value="Enviar"></div>
	        </form>
		</div>
	</div>


</body>

</html>