<?php
		session_start();
		 $_SESSION['nombreAd'];
		 $_SESSION['numCuenta'];
		
?>

<!DOCTYPE HTML>

<html lang= "es">
<head>
    <title>Información</title>
	<meta charset="utf-8">
	<link rel= "stylesheet" type="text/css" href="estilo.css">
</head>


<body id="principal">

 <header>
    <div class="banda">
	    <span class="inline">
		    <img src= "https://cdn2.cocinadelirante.com/sites/default/files/styles/gallerie/public/images/2019/08/receta-mazapan-para-pastel.jpg" width="210" height="150">
			<img src= "https://www.flores.ninja/wp-content/uploads/2017/05/Flores.jpg" width="210" height="150">
			<img src= "https://i.pinimg.com/originals/73/73/ca/7373ca91e50abae367ad5d0835777191.jpg" width="205" height="150">
			<img src= "https://66.media.tumblr.com/33bbdf066c07f915615d85e7ff8d72c8/tumblr_mosenvL9TP1snihqzo1_500.jpg" width="210" height="150">
			<img src= "https://www.nicepng.com/png/detail/37-373513_clipart-resolution-15001500-imagenes-de-tumblr-mariposas.png" width="206" height="150">
			<img src= "https://static.vix.com/es/sites/default/files/styles/m/public/l/luna-rosa-llena.jpg" width="206" height="150">
			<img src= "https://previews.123rf.com/images/astronight/astronight1708/astronight170800002/83523617-hermosa-ilustraci%C3%B3n-vectorial-con-fresa-fondo-de-fresa-rosa-patrones-sin-fisuras-con-frutas-de-jard%C3%ADn-fon.jpg" width="206" height="150">
		</span>
		<div class="block">
	    <a href="formulario.php" type="submit" class="button_active" value="Registro">Registro de Alumnos</a>
		<a href="info.php" type="submit" class="button_active" value="Tablero">Mostrar Alumnos</a>
		<a href="login.php" type="submit" class="button_active" value="Registro">Cerrar sesión</a>
		</div>
	</div>
</header>


    <div class="container">
	    <h1 align="center">Información del Usuario</h1>
    	<div id="container_usuario">
		    <h2>Bienvenido: <?php echo $_SESSION['nombreAd'];?></h2>
			<p>Número de cuenta: <?php echo $_SESSION['numCuenta'];?></p>
        </div>
		<h1 align="center">Alumnos Inscritos</h1>
		<div id="container_alumnos">
		    <table>
			<tr>
			    <th>&nbsp;&nbsp;&nbsp;Nombre &nbsp;&nbsp;&nbsp;</th>
				<th>&nbsp;&nbsp;&nbsp;Apellido Paterno &nbsp;&nbsp;&nbsp;</th>
				<th>&nbsp;&nbsp;&nbsp;Apellido Materno &nbsp;&nbsp;&nbsp;</th>
				<th>&nbsp;&nbsp;&nbsp;Número de Cuenta  &nbsp;&nbsp;&nbsp;</th>
				<th>&nbsp;&nbsp;&nbsp;Genero &nbsp;&nbsp;&nbsp;</th>
				<th>&nbsp;&nbsp;&nbsp;Fecha Nacimiento &nbsp;&nbsp;&nbsp;</th>
			</tr>
			<tr>
			<?php if($_SESSION['alumnos']){ ?>
				<?php foreach($_SESSION['alumnos'] as $row){ ?>
				<tr>
			    <td><?php echo $row['nombre'];?></td>
			    <td><?php echo $row['num_cta'];?></td>
			    <td><?php echo $row['primer_apellido'];?></td>
			    <td><?php echo $row['segundo_apellido'];?></td>
			    <td><?php echo $row['genero'];?></td>
			    <td><?php echo $row['fec_nac'];?></td>
				</tr>
				<?php } ?>
				
			<?php } ?>
			</tr>
			
			
             
            </table>
        </div>
    </div>


</body>
</html>