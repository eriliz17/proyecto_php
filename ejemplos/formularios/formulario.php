<html>
<head>
    <title>Formularios</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
    <link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
    <link rel="stylesheet"
          href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">
</head>
    <body>
	<div class="container">
		<div class="columns">
			<!--
				En la etiqueta form son importantes los siguientes atributos method y action
				method para especificar el método en que se va a enviar el formulario "get" o "post" cambien el tipo de metodo para ver cambios
				action para especificar el archivo que va a procesar la información
			-->
			<form action="procesar_formulario.php?accion=get&texto=textoenget" method="POST">
				<!-- form text control -->
				<label class="form-label" for="input-text">Nombre</label>
				<!--
					Cuando se procesa en PHP el formulario este los transforma en un arreglo asociativo
					donde la llave sera el name del input y el valor seta el valor del input
				-->
				<input name="texto" class="form-input " type="text" id="input-nombre" placeholder="Nombre">
				<!-- form password control -->
				<label class="form-label" for="input-password">Contraseña</label>
				<input name="password" class="form-input" type="password" id="input-password"
					   placeholder="Contraseña">

				<!-- form textarea control -->
				<label class="form-label" for="input-textarea">Mensaje</label>
				<textarea name="textarea" class="form-input" id="input-textarea"
						  placeholder="Textarea" rows="3"></textarea>

				<!-- form select control -->
				<label class="form-label" for="input-select">¿Que programa usas?</label>
				<select id="input-select" class="form-select" name="red_social">
					<option value=''>--Elige uno--</option>
					<option value='Sk'>Skype</option>
					<option value='Te'>Telegram</option>
					<option value='Tw'>Twitter</option>
					<option value='Wh'>Whatsapp</option>
				</select>

				<!-- form radio control -->
				<label class="form-label">Sexo</label>
				<label class="form-radio">
					<input type="radio" name="sexo" value="H" checked>
					<i class="form-icon"></i> Hombre
				</label>
				<label class="form-radio">
					<input type="radio" name="sexo" value="M">
					<i class="form-icon"></i> Mujer
				</label>

				<!-- form checkbox control -->
				<label class="form-label">Pasatiempos</label>
				<label class="form-checkbox">
					<input type="checkbox" name="pasatiempo" value="Videojuegos">
					<i class="form-icon"></i> Videojuegos </input>
				</label>
				<label class="form-checkbox">
					<input type="checkbox" name="pasatiempo" value="Deportes">
					<i class="form-icon"></i> Deportes </input>
				</label>
				<label class="form-checkbox">
					<input type="checkbox" name="pasatiempo" value="Lectura">
					<i class="form-icon"></i> Lectura </input>
				</label>

				<!-- HTML 5 controls -->
				<!-- form date control -->
				<label class="form-label" for="input-date">Fecha</label>
				<input name="date" class="form-input " type="date" id="input-date"
					   placeholder="fecha">

				<!-- form date control -->
				<label class="form-label" for="input-email">Email</label>
				<input name="email" class="form-input " type="email" id="input-date"
					   placeholder="Correo">

				<!-- Botones -->
				<input type='submit' class="btn" value="Enviar"/>
				<input type='reset' class="btn btn-primary" value="limpiar"/>
			</form>
		</div>
	</div>
    </body>
</html>